package org.eclipse.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */

public class TestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

//		String nom = request.getParameter("nom");
//		String prenom = request.getParameter("prenom");
//		PrintWriter out =  response.getWriter();
//		out.println("Hello " + nom + " " + prenom) ;

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String param1 = request.getParameter("param1");
		if (param1 == null) {
			param1 = "0";
		}
		String param2 = request.getParameter("param2");
		if (param2 == null) {
			param2 = "0";
		}
		out.println("le r�sultat de l'addition est " + (Integer.parseInt(param1) + Integer.parseInt(param2)) + "<br>");
		out.println(
				"le r�sultat de la soustraction est " + (Integer.parseInt(param1) - Integer.parseInt(param2)) + "<br>");
		out.println("le r�sultat de la multiplication est " + (Integer.parseInt(param1) * Integer.parseInt(param2))
				+ "<br>");
		if (param1.equals("0") || param2.equals("0")) {
			out.println("Division impossible : pas de division par 0");
		} else {
			out.println(
					"le r�sultat de la division est " + (Integer.parseInt(param1) / Integer.parseInt(param2)) + "<br>");
		}
//		out.println("<!Doctype html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"utf-8\" />");
//		out.println("<title>Projet JEE</title>");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("Hello Wolrd");
//		out.println("Hello Wolrd");
//		out.println("Hello Wolrd");
//		out.println("</body>");
//		out.println("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		String param1 = request.getParameter("param1");
		if (param1 == null) {
			param1 = "0";
		}
		String param2 = request.getParameter("param2");
		if (param2 == null) {
			param2 = "0";
		}
		String add = request.getParameter("addition");
		String sous = request.getParameter("soustraction");
		String mul = request.getParameter("multiplication");
		String div = request.getParameter("division");
		out.println("le r�sultat de l'addition est " + (Integer.parseInt(param1) + Integer.parseInt(param2)) + "<br>");
		out.println("le r�sultat de la soustraction est " + (Integer.parseInt(param1) - Integer.parseInt(param2)) + "<br>");
		out.println("le r�sultat de la multiplication est " + (Integer.parseInt(param1) * Integer.parseInt(param2)) + "<br>");
		
		if (param1.equals("0") || param2.equals("0")) {
			out.println("Division impossible : pas de division par 0");
		} else {
			out.println(
					"le r�sultat de la division est " + (Integer.parseInt(param1) / Integer.parseInt(param2)) + "<br>");
		}

	}
}
